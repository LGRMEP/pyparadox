=======
Credits
=======

Development Lead
----------------

* Carmen Bianca Bakker <c.b.bakker@carmenbianca.eu>

Contributors
------------

* Robin C. Thomas <rc.thomas90@gmail.com> - Code snippets from ck2launcher
