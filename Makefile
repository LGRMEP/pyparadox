.PHONY: clean clean-build clean-pyc clean-test docs dist

help:
	@echo "clean - remove all build, test, coverage and Python artifacts"
	@echo "clean-build - remove build artifacts"
	@echo "clean-pyc - remove Python file artifacts"
	@echo "clean-test - remove test and coverage artifacts"
	@echo "lint - check style with flake8"
	@echo "test - run tests quickly with the default Python"
	@echo "test-all - run tests on every Python version with tox"
	@echo "link-pyqt - Link PyQt libraries to your virtualenv"
	@echo "coverage - check code coverage quickly with the default Python"
	@echo "docs - generate Sphinx HTML documentation, including API docs"
	@echo "release - package and upload a release"
	@echo "dist - build the package"
	@echo "install - install the package to the active Python's site-packages"
	@echo "develop - install the package as .egg-link to source directory"

clean: clean-build clean-pyc clean-test

clean-build:
	rm -fr build/
	rm -fr docs/_build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test:
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -f coverage.xml

lint:
	flake8 pyparadox tests

test:
	xvfb-run python3 setup.py test

test-all:
	tox

link-pyqt:
	/usr/bin/python3 link_pyqt.py ${VIRTUAL_ENV} 5

coverage:
	xvfb-run coverage run --source pyparadox setup.py test
	coverage report -m
	coverage html
	coverage xml

docs:
	rm -f docs/pyparadox.rst
	rm -f docs/modules.rst
	sphinx-apidoc -o docs/ pyparadox
	$(MAKE) -C docs clean
	$(MAKE) -C docs html

release: clean
	python3 setup.py sdist upload
	python3 setup.py bdist_wheel upload

dist: clean
	python3 setup.py sdist
	python3 setup.py bdist_wheel
	ls -l dist

install: clean
	python3 setup.py install

develop: clean
	python3 setup.py develop
